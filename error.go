package sockjsclient

import "errors"

// errNoAddressProvided is returned if no address is provided to GetServerInfo().
var errNoAddressProvided = errors.New("sockjsclient: no address provided")

// IsNotConnected will return if this is a client / conn not connected error
func IsNotConnected(err error) bool {
	return errors.Is(err, ErrClosedConnection) || errors.Is(err, ErrClientNotConnected)
}
