module codeberg.org/gruf/go-sockjsclient

go 1.16

require (
	codeberg.org/gruf/go-format v1.0.3
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/gorilla/websocket v1.4.2
	github.com/igm/sockjs-go/v3 v3.0.1
)
