package sockjsclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"unicode"
	"unicode/utf16"
	"unicode/utf8"
)

// MessageType represents a sockjs message type
type MessageType uint8

// Sockjs message types (0th represents any unhandled, treated as error)
const (
	MessageTypeUnhandled = MessageType(iota)
	MessageTypeHeartbeat = MessageType(iota)
	MessageTypeData      = MessageType(iota)
	MessageTypeOpen      = MessageType(iota)
	MessageTypeClose     = MessageType(iota)
)

func DecodeResponse(b []byte) (MessageType, [][]byte, error) {
	// Trim any whitespace from data
	b = bytes.TrimSpace(b)

	if len(b) > 0 {
		// We have non-zero input data

		switch b[0] {
		// Heartbeat
		case 'h':
			if len(b) == 1 {
				return MessageTypeHeartbeat, nil, nil
			}

		// Normal message
		case 'a':
			msgs, err := UnmarshalMessages(b[1:])
			return MessageTypeData, msgs, err

		// Session open
		case 'o':
			if len(b) == 1 {
				return MessageTypeOpen, nil, nil
			}

		// Session closed
		case 'c':
			err := decodeCloseError(b[1:])
			return MessageTypeClose, nil, err
		}
	}

	// If we reached here, this is invalid data
	return MessageTypeUnhandled, nil, errorf("%w: invalid message %q", ErrInvalidResponse, b)
}

// decodeCloseError will attempt to parse a close error from msg data.
func decodeCloseError(data []byte) error {
	var v []interface{}
	if err := json.Unmarshal(data, &v); err == nil && len(v) == 2 {
		if len(v) == 2 {
			return errorf("%w (%v, %v)", ErrClosedByRemote, v[0], v[1])
		}
	}
	return errorf("%w: invalid close message %q", ErrClosedByRemote, data)
}

// emptyMsgs is a preallocated JSON empty msgs list.
var emptyMsgs = []byte("[]")

// MarshalMessages will marshal a slice of messages into a JSON payload.
func MarshalMessages(msgs ...[]byte) []byte {
	switch len(msgs) {
	case 0:
		return emptyMsgs
	case 1:
		b := make([]byte, 1, 2)
		b[0] = '['
		b = append(b, marshalMsg(msgs[0])...)
		b = append(b, ']')
		return b
	default:
		// Start the msgs list
		b := make([]byte, 1, len(msgs))
		b[0] = '['

		// Append each of the msgs
		b = append(b, marshalMsg(msgs[0])...)
		for i := 1; i < len(msgs); i++ {
			b = append(b, ',')
			b = append(b, marshalMsg(msgs[i])...)
		}

		// Close the list
		b = append(b, ']')
		return b
	}
}

// UnmarshalMessages will unmarshal a JSON payload into a slice of messages.
func UnmarshalMessages(b []byte) ([][]byte, error) {
	// Check for valid input
	if len(b) < 2 {
		return nil, errors.New("invalid json")
	}

	// Check for nil JSON
	if string(b) == "null" {
		return nil, nil
	}

	// Check for valid JSON array
	if b[0] != '[' || b[len(b)-1] != ']' {
		return nil, errors.New("invalid json")
	}

	// Skip past braces
	b = b[1 : len(b)-1]

	msgs := [][]byte{}

	// Check for empty
	if len(b) < 1 {
		return msgs, nil
	}

	for {
		// Look for opening quote
		if b[0] != '"' {
			return nil, errors.New("invalid json")
		}

		// Skip '"'
		b = b[1:]

		var i int
	inner:
		for {
			// Look for next quote char
			i += bytes.IndexByte(b[i:], '"')
			if i < 0 {
				return nil, errors.New("invalid json")
			}

			n := 0

			// Count preceding slashes
			for j := i - 1; j >= 0 && b[j] == '\\'; j-- {
				n++
			}

			// If even, i.e. NOT escaped,
			// this is a valid end quote
			if n%2 == 0 {
				break inner
			}

			// Skip past
			i++
		}

		// Reslice
		msg := b[:i]
		b = b[i+1:]

		// Unmarshal JSON encoded string
		msg, err := unmarshalMsg(msg)
		if err != nil {
			return nil, err
		}

		// Append parsed
		msgs = append(msgs, msg)

		// Check for finish
		if len(b) < 1 {
			return msgs, nil
		}

		// Check for valid remain
		if len(b) < 2 || b[0] != ',' {
			return nil, errors.New("invalid json")
		}

		// Skip comma
		b = b[1:]
	}
}

// jsonEscape is a lookup table of chars requiring JSON escape.
var jsonEscape = [256]bool{
	0:    true,
	1:    true,
	2:    true,
	3:    true,
	4:    true,
	5:    true,
	6:    true,
	7:    true,
	8:    true,
	9:    true,
	10:   true,
	11:   true,
	12:   true,
	13:   true,
	14:   true,
	15:   true,
	16:   true,
	17:   true,
	18:   true,
	19:   true,
	20:   true,
	21:   true,
	22:   true,
	23:   true,
	24:   true,
	25:   true,
	26:   true,
	27:   true,
	28:   true,
	29:   true,
	30:   true,
	31:   true,
	'"':  true,
	'\\': true,
}

// marshalMsg will marshal a byte slice as JSON-escaped string.
func marshalMsg(b []byte) []byte {
	const chars = "0123456789abcdef"

	// Start the msg buffer
	buf := make([]byte, 1, len(b))
	buf[0] = '"'

	// Last non-escape symbol
	p := 0

	for i := 0; i < len(b); {
		c := b[i]

		if c < utf8.RuneSelf {
			// Check if escaping needed
			if !jsonEscape[c] {
				i++
				continue
			}

			buf = append(buf, b[p:i]...)
			switch c {
			case '\t':
				buf = append(buf, '\t')
			case '\r':
				buf = append(buf, '\r')
			case '\n':
				buf = append(buf, '\n')
			case '\\':
				buf = append(buf, `\\`...)
			case '"':
				buf = append(buf, `\"`...)
			default:
				buf = append(buf, `\u00`...)
				buf = append(buf, chars[c>>4], chars[c&0xf])
			}

			i++
			p = i
			continue
		}

		// Decode next rune in string
		r, w := utf8.DecodeRune(b[i:])

		// Check for broken UTF
		if r == utf8.RuneError && w == 1 {
			buf = append(buf, b[p:i]...)
			buf = append(buf, `\ufffd`...)
			i++
			p = i
			continue
		}

		// jsonp stuff - tab separator and line separator
		if r == '\u2028' || r == '\u2029' {
			buf = append(buf, b[p:i]...)
			buf = append(buf, `\u202`...)
			buf = append(buf, chars[r&0xf])
			i += w
			p = i
			continue
		}

		i += w
	}

	// Append remaining bytes
	buf = append(buf, b[p:]...)
	buf = append(buf, '"')
	return buf
}

// unmarshalMsg will unmarshal a JSON-escaped string to byte slice.
func unmarshalMsg(b []byte) ([]byte, error) {
	var msg []byte

	i := 0
	for {
		// Check for next escape seq
		idx := bytes.IndexByte(b[i:], '\\')
		if idx < 0 {
			// Nothing to escape
			if i == 0 {
				return b, nil
			}

			// Finished
			break
		}

		// Set abs idx
		idx += i

		// Valid escape requires >= 2 chars
		if idx >= len(b)-1 {
			return nil, errors.New("invalid escape")
		}

		// Decode next escaped rune
		r, n, err := decodeEscape(b[idx:])
		if err != nil {
			return nil, err
		}

		// Append up to escaped
		msg = append(msg, b[i:idx]...)

		var d [4]byte

		// Encode and append rune bytes
		m := utf8.EncodeRune(d[:], r)
		msg = append(msg, d[:m]...)

		// Skip rune
		i = idx + n
	}

	// Append remaining bytes
	msg = append(msg, b[i:]...)

	return msg, nil
}

// decodeEscape will decode an escaped rune, returning length.
func decodeEscape(b []byte) (rune, int, error) {
	switch c := b[1]; c {
	case '"', '/', '\\':
		return rune(c), 2, nil
	case 'b':
		return '\b', 2, nil
	case 'f':
		return '\f', 2, nil
	case 'n':
		return '\n', 2, nil
	case 'r':
		return '\r', 2, nil
	case 't':
		return '\t', 2, nil
	case 'u':
		r := rune(-1)

		// Decode rune
		if len(b) >= 6 {
			r = getu4(b)
		}

		// Check rune
		if r < 0 {
			return 0, 0, errors.New("incorrectly escaped \\uXXXX sequence")
		}

		n := 6

		// Look for 1st in surrogate pair
		if utf16.IsSurrogate(r) {
			var r2 rune

			b2 := b[6:]

			// Decode next rune (if poss)
			if len(b2) < 6 || b2[0] != '\\' || b2[1] != 'u' {
				r2 = getu4(b2)
			}

			// Try to decode a surrogate pair from 2 runes
			if rr := utf16.DecodeRune(r, r2); rr != unicode.ReplacementChar {
				n += 6
				r = rr
			} else {
				// Invalid 1 of surrogate pair
				r = unicode.ReplacementChar
			}
		}

		return r, n, nil
	}

	return 0, 0, errors.New("incorrectly escaped bytes")
}

// hex2Rune is a lookup table of hex chars to their integer value.
var hex2Int = func() [256]int8 {
	// hex chars as values
	v := [256]int8{
		'0': 0,
		'1': 1,
		'2': 2,
		'3': 3,
		'4': 4,
		'5': 5,
		'6': 6,
		'7': 7,
		'8': 8,
		'9': 9,
		'a': 10,
		'A': 10,
		'b': 11,
		'B': 11,
		'c': 12,
		'C': 12,
		'd': 13,
		'D': 13,
		'e': 14,
		'E': 14,
		'f': 15,
		'F': 15,
	}

	// Everything < '0' updated to -1
	for i := 0; i < '0'; i++ {
		v[i] = -1
	}

	// All others >= '1' that aren't
	// set also updated to -1
	for i := '1'; i < 256; i++ {
		if v[i] == 0 {
			v[i] = -1
		}
	}

	return v
}()

// getu4 decodes \uXXXX from the beginning of s, returning the hex value or -1.
func getu4(s []byte) rune {
	var val rune
	for i := 2; i < len(s) && i < 6; i++ {
		// Get hex int value
		c := hex2Int[s[i]]
		if c < 0 {
			return -1
		}

		// Update value
		val <<= 4
		val |= rune(c)
	}
	return val
}
