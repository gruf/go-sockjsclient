# go-sockjsclient

Written by myself during my time at Third Light.

This is a fork with some rather intense optimizations:

- minimising bytes <-> string conversion allocations

- manual marshal / unmarshal of JSON `[]string` message arrays