package sockjsclient

import (
	"errors"
)

// Sockjs client connection error messages
var (
	ErrClosedConnection   = errors.New("sockjsclient: use of a closed connection")
	ErrClosedByRemote     = errors.New("sockjsclient: connection closed by remote")
	ErrClosingConnection  = errors.New("sockjsclient: error closing connection")
	ErrInvalidResponse    = errors.New("sockjsclient: invalid server response")
	ErrUnexpectedResponse = errors.New("sockjsclient: unexpected server response")
	ErrNoHeartbeat        = errors.New("sockjsclient: no heartbeat")
)

// Conn represents a sockjs client connection
type Conn interface {
	// ReadMsg reads the next single data message from the sockjs connection
	ReadMsg() ([]byte, error)

	// WriteMsg writes a block of data messages to the sockjs connection
	WriteMsgs(...[]byte) error

	// Close will close the sockjs connection
	Close() error
}
