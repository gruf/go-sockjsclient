package sockjsclient

import (
	"context"
	"crypto/rand"
	"errors"
	"fmt"
	"math"
	"math/big"
	"net"
	"net/url"
	"path"

	"github.com/gorilla/websocket"
)

// errorf is shorthand for fmt.Errorf().
func errorf(s string, a ...interface{}) error {
	return fmt.Errorf(s, a...)
}

// parseTransportAddr parses a valid transport address from given base address
// server ID and client session ID. ALL of these must be provided
func parseTransportAddr(addr, serverID, sessionID string) (string, error) {
	// Ensure valid addr
	url, err := url.Parse(addr)
	if err != nil {
		return "", err
	}

	// Ensure IDs are set
	if serverID == "" {
		return "", errors.New("sockjsclient: no server id provided")
	}
	if sessionID == "" {
		return "", errors.New("sockjsclient: no session id provided")
	}

	// Prepare transport address
	taddr := path.Join(url.Host, url.Path, serverID, sessionID)
	if url.Scheme != "" {
		taddr = url.Scheme + "://" + taddr
	}

	return taddr, nil
}

// maskCtxCancelled replaces any context cancelled/timeout errors with ErrClosedConnection
func maskCtxCancelled(ctx context.Context, err error) error {
	if errors.Is(err, ctx.Err()) {
		return ErrClosedConnection
	}
	return err
}

// isWebsocketClosed will check if this received websocket error indicates a closed connection
func isWebsocketClosed(err error) bool {
	switch {
	case errors.Is(err, websocket.ErrCloseSent),
		errors.Is(err, net.ErrClosed):
		return true
	default:
		e := &websocket.CloseError{}
		return errors.As(err, &e)
	}
}

// paddedRandomIntn returns a string representation of a padded random int up-to max
func paddedRandomIntn(max int) string {
	n := math.Log10(float64(max))
	i, err := rand.Int(rand.Reader, big.NewInt(int64(max)))
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%0[1]d", i.Int64(), int(n))
}
